
class Node(object):
    """Node in the search tree. It's used in sets,
    so it needs  __eq__ and __hash__ functions.
    """
    def __init__(self, loc, parent=None):
        """Create new node. If parent is not present this is a
        start of the search tree. Else this node is reached from
        its parent
        """
        self.loc = loc
        self.parent = parent
        if parent:
            self.cost = parent.cost + 1
        else:
            self.cost = 0
        self.heuristic = 0

    def __eq__(self, obj):
        if self.loc == obj.loc:
            return True
        return False

    def __hash__(self):
        return hash(self.loc)

    @property
    def rating(self):
        return self.cost + self.heuristic


class AStar(object):
    """Path finder using A* algorithm as defined in ai-class.com
    in Unit 2 "Problem Solving". Works on boards where cost of every
    move from one field to another is equal 1. Used heuristic is
    a distance between two fields on the board in taxi metric.
    """

    def __init__(self, board):
        """Board is an object representing two dimensional board. It should
        implement methods:
        - distance(a, b) - taxi metric distance between two fields 
        on the board
        - neighbours(a) - fields available to direct move from field a
        """
        self.board = board


    def _compute_heuristics(self):
        """Compute distances from nodes in frontier to destination field.

        Return node with smallest (the best) rating.
        """
        for node in self.frontier:
            best_node = node
            break
        for node in self.frontier:
            if not node.heuristic:
                node.heuristic = self.board.distance(node.loc, self.dest)
            if node.rating < best_node.rating:
                best_node = node
        return best_node

    def _resolve_path(self, node):
        """Return path from start to the node"""
        path = []
        while node.parent:
            node = node.parent
            path.append(node.loc)
        path.reverse()
        return path

    def find_path(self, loc, dest):
        """Find path using A* from loc to dest"""
        self.dest = dest
        start = Node(loc)
        self.frontier = set([Node(location, start) \
                for location in self.board.neighbours(loc)])
        explored = set([loc,])
        while True:
            if not self.frontier:
                return []
            node = self._compute_heuristics()
            self.frontier.remove(node)
            explored.add(node.loc)
            if node.loc == dest:
                return self._resolve_path(node)
            for new_loc in self.board.neighbours(node.loc):
                if new_loc not in explored:
                    self.frontier.add(Node(new_loc, node))
