import unittest
import astar

class Board(object):
    """Rectangural board (example chessboard, represented as sequence of 
    strings. Each field is either O (passable field) or X (not passable). 
    Each string is a row, each character in it is a column (so all strings in
    the sequence should have equal length).

    Field is a tuple: (row, col). Rows and cols are enumerated from 0.
    """
    def __init__(self, strings):
        self._board = strings
        self._rows = len(strings)
        self._cols = len(strings[0])

    def _passable(self, loc):
        """Chceck if field is passable"""
        return True if self._board[loc[0]][loc[1]] == 'O' else False

    def distance(self, loc1, loc2):
        """Get closesest distance (taxi metric) between two locations.
        Non-passable fields are treated as passable here.
        """
        row1, col1 = loc1
        row2, col2 = loc2
        d_col = min(abs(col1 - col2), self._cols - abs(col1 - col2))
        d_row = min(abs(row1 - row2), self._rows - abs(row1 - row2))
        return d_row + d_col

    def neighbours(self, loc):
        """Get neighbours of the loc (no diagonal moves)"""
        row, col = loc
        rows = self._rows
        cols = self._cols
        result = filter(lambda x: self._passable(x), (((row - 1) % rows, col),
            (row, (col + 1) % cols),
            ((row + 1) % rows, col),
            (row, (col - 1) % cols))) 
        return result


class TestAStar(unittest.TestCase):
    """Test A* algorithm on 2-dimensional board"""
    def setUp(self):
        board_map = (
            'OOOOOOOOOOOO',
            'OOXXXXXXXOOO',
            'OOOOOOOOXOOO',
            'OOOOOOOOXOOO',
            'OOOOOOXXXOOO',
            'OOOOOOOOXOOO',
            'OOOOOOOOOOOO',
            'OOOOOOOOOOOO',
            'OOOOOOOOOOOO',
            'OOOOOOOOOOOO',
        )
        self.board = Board(board_map)
        self.finder = astar.AStar(self.board)

    def test_board(self):
        self.assertEqual(self.board.distance((0, 8), (0, 0)), 4)
        self.assertEqual(self.board.distance((3, 2), (6, 3)), 4)
        self.assertEqual(self.board.distance((7, 5), (1, 3)), 6)

        self.assertEqual(set(self.board.neighbours((9, 11))),
                set([(8, 11), (9, 10), (0, 11), (9, 0)]))
        self.assertEqual(set(self.board.neighbours((2, 7))),
                set([(3, 7), (2, 6)]))
        self.assertEqual(set(self.board.neighbours((4, 5))),
                set([(5, 5), (3, 5), (4, 4)]))
        self.assertEqual(set(self.board.neighbours((0, 2))),
                set([(0, 3), (0, 1), (9, 2)]))

        self.assertTrue(self.board._passable((0, 0)))
        self.assertTrue(self.board._passable((7, 5)))
        self.assertFalse(self.board._passable((1, 5)))
        self.assertFalse(self.board._passable((5, 8)))

    def test_astar(self):
        path = self.finder.find_path((2, 5), (2, 9))
        self.assertEqual(path, [(2, 5), (2, 4), (2, 3), (2, 2), (2, 1), (2, 0), (2, 11), (2, 10)])



if __name__ == '__main__':
    unittest.main()
